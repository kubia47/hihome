<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Eposi_Theme
 * @since Eposi 1.0
 */
$eposi_opt = get_option( 'eposi_opt' );
get_header();
$eposi_bloglayout = 'sidebar';
if(isset($eposi_opt['blog_layout']) && $eposi_opt['blog_layout']!=''){
	$eposi_bloglayout = $eposi_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$eposi_bloglayout = $_GET['layout'];
}
$eposi_blogsidebar = 'right';
if(isset($eposi_opt['sidebarblog_pos']) && $eposi_opt['sidebarblog_pos']!=''){
	$eposi_blogsidebar = $eposi_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$eposi_blogsidebar = $_GET['sidebar'];
}
if ( !is_active_sidebar( 'sidebar-1' ) )  {
	$eposi_bloglayout = 'nosidebar';
}
$eposi_blog_main_extra_class = NULl;
if($eposi_blogsidebar=='left') {
	$eposi_blog_main_extra_class = 'order-lg-last';
}
$main_column_class = NULL;
switch($eposi_bloglayout) {
	case 'sidebar':
		$eposi_blogclass = 'blog-sidebar';
		$eposi_blogcolclass = 9;
		$main_column_class = 'main-column';
		Eposi_Class::eposi_post_thumbnail_size('eposi-post-thumb');
		break;
	case 'largeimage':
		$eposi_blogclass = 'blog-large';
		$eposi_blogcolclass = 9;
		$main_column_class = 'main-column';
		Eposi_Class::eposi_post_thumbnail_size('eposi-post-thumbwide');
		break;
	case 'grid':
		$eposi_blogclass = 'grid';
		$eposi_blogcolclass = 9;
		$main_column_class = 'main-column';
		Eposi_Class::eposi_post_thumbnail_size('eposi-post-thumbwide');
		break;
	default:
		$eposi_blogclass = 'blog-nosidebar';
		$eposi_blogcolclass = 12;
		$eposi_blogsidebar = 'none';
		Eposi_Class::eposi_post_thumbnail_size('eposi-post-thumb');
}
?>
<div class="main-container">
	<div class="title-breadcumbs">
		<div class="container">
			<header class="entry-header">
				<h2 class="entry-title"><?php printf( wp_kses(__( 'Search Results for: %s', 'eposi' ), array('span'=>array())), '<span>' . get_search_query() . '</span>' ); ?></h2>
			</header>
			<div class="breadcrumb-container">
				<?php Eposi_Class::eposi_breadcrumb(); ?> 
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12 <?php echo 'col-lg-'.$eposi_blogcolclass; ?> <?php echo ''.$main_column_class; ?> <?php echo esc_attr($eposi_blog_main_extra_class);?>">
				<div class="page-content blog-page blogs <?php echo esc_attr($eposi_blogclass); if($eposi_blogsidebar=='left') {echo ' left-sidebar'; } if($eposi_blogsidebar=='right') {echo ' right-sidebar'; } ?>">
					<?php if ( have_posts() ) : ?>
						<div class="post-container">
							<?php /* Start the Loop */ ?>
							<?php while ( have_posts() ) : the_post(); ?>
								<?php get_template_part( 'content', get_post_format() ); ?>
							<?php endwhile; ?>
						</div>
						<?php Eposi_Class::eposi_pagination(); ?>
					<?php else : ?>
						<article id="post-0" class="post no-results not-found">
							<header class="entry-header">
								<h1 class="entry-title"><?php esc_html_e( 'Nothing Found', 'eposi' ); ?></h1>
							</header>
							<div class="entry-content">
								<p><?php esc_html_e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'eposi' ); ?></p>
								<?php get_search_form(); ?>
							</div><!-- .entry-content -->
						</article><!-- #post-0 -->
					<?php endif; ?>
				</div>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
	<!-- brand logo -->
	<?php 
		if(isset($eposi_opt['inner_brand']) && function_exists('eposi_brands_shortcode') && shortcode_exists( 'ourbrands' ) ){
			if($eposi_opt['inner_brand'] && isset($eposi_opt['brand_logos'][0]) && $eposi_opt['brand_logos'][0]['thumb']!=null) { ?>
				<div class="inner-brands">
					<div class="container">
						<?php echo do_shortcode('[ourbrands]'); ?>
					</div>
				</div>
			<?php }
		}
	?>
	<!-- end brand logo --> 
</div>
<?php get_footer(); ?>