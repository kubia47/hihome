"use strict";
// product-magnifier var
var eposi_magnifier_vars;
var yith_magnifier_options = {
		
		sliderOptions: {
			responsive: eposi_magnifier_vars.responsive,
			circular: eposi_magnifier_vars.circular,
			infinite: eposi_magnifier_vars.infinite,
			direction: 'left',
			debug: false,
			auto: false,
			align: 'left',
			height: 'auto',
			prev    : {
				button  : "#slider-prev",
				key     : "left"
			},
			next    : {
				button  : "#slider-next",
				key     : "right"
			},
			scroll : {
				items     : 1,
				pauseOnHover: true
			},
			items   : {
				visible: Number(eposi_magnifier_vars.visible),
			},
			swipe : {
				onTouch:    true,
				onMouse:    true
			},
			mousewheel : {
				items: 1
			}
		},
		
		showTitle: false,
		zoomWidth: eposi_magnifier_vars.zoomWidth,
		zoomHeight: eposi_magnifier_vars.zoomHeight,
		position: eposi_magnifier_vars.position,
		lensOpacity: eposi_magnifier_vars.lensOpacity,
		softFocus: eposi_magnifier_vars.softFocus,
		adjustY: 0,
		disableRightClick: false,
		phoneBehavior: eposi_magnifier_vars.phoneBehavior,
		loadingLabel: eposi_magnifier_vars.loadingLabel,
	};