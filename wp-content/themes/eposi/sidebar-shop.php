<?php
/**
 * The sidebar for shop page
 *
 * If no active widgets are in the sidebar, hide it completely.
 *
 * @package WordPress
 * @subpackage Eposi_Theme
 * @since Eposi 1.0
 */
$eposi_opt = get_option( 'eposi_opt' );
$shopsidebar = 'left';
if(isset($eposi_opt['sidebarshop_pos']) && $eposi_opt['sidebarshop_pos']!=''){
	$shopsidebar = $eposi_opt['sidebarshop_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$shopsidebar = $_GET['sidebar'];
}
$eposi_shop_sidebar_extra_class = NULl;
if($shopsidebar=='left') {
	$eposi_shop_sidebar_extra_class = 'order-lg-first';
}
?>
<?php if ( is_active_sidebar( 'sidebar-shop' ) ) : ?>
	<div id="secondary" class="col-12 col-lg-3 sidebar-shop <?php echo esc_attr($eposi_shop_sidebar_extra_class);?>">
		<div class="secondary-inner">
			<?php dynamic_sidebar( 'sidebar-shop' ); ?>
		</div>
	</div>
<?php endif; ?>