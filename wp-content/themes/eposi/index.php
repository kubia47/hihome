<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Eposi_Theme
 * @since Eposi 1.0
 */
$eposi_opt = get_option( 'eposi_opt' );
get_header();
$eposi_bloglayout = 'sidebar';
if(isset($eposi_opt['blog_layout']) && $eposi_opt['blog_layout']!=''){
	$eposi_bloglayout = $eposi_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$eposi_bloglayout = $_GET['layout'];
}
$eposi_blogsidebar = 'right';
if(isset($eposi_opt['sidebarblog_pos']) && $eposi_opt['sidebarblog_pos']!=''){
	$eposi_blogsidebar = $eposi_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$eposi_blogsidebar = $_GET['sidebar'];
}
if ( !is_active_sidebar( 'sidebar-1' ) )  {
	$eposi_bloglayout = 'nosidebar';
}
$eposi_blog_main_extra_class = NULl;
if($eposi_blogsidebar=='left') {
	$eposi_blog_main_extra_class = 'order-lg-last';
}
$main_column_class = NULL;
switch($eposi_bloglayout) {
	case 'sidebar':
		$eposi_blogclass = 'blog-sidebar';
		$eposi_blogcolclass = 9;
		$main_column_class = 'main-column';
		Eposi_Class::eposi_post_thumbnail_size('eposi-post-thumb');
		break;
	case 'largeimage':
		$eposi_blogclass = 'blog-large';
		$eposi_blogcolclass = 9;
		$main_column_class = 'main-column';
		Eposi_Class::eposi_post_thumbnail_size('eposi-post-thumbwide');
		break;
	case 'grid':
		$eposi_blogclass = 'grid';
		$eposi_blogcolclass = 9;
		$main_column_class = 'main-column';
		Eposi_Class::eposi_post_thumbnail_size('eposi-post-thumbwide');
		break;
	default:
		$eposi_blogclass = 'blog-nosidebar';
		$eposi_blogcolclass = 12;
		$eposi_blogsidebar = 'none';
		Eposi_Class::eposi_post_thumbnail_size('eposi-post-thumb');
}
?>
<div class="main-container">
	<div class="title-breadcumbs">
		<div class="container">
			<header class="entry-header">
				<h2 class="entry-title"><?php if(isset($eposi_opt['blog_header_text']) && ($eposi_opt['blog_header_text'] !='')) { echo esc_html($eposi_opt['blog_header_text']); } else { esc_html_e('Blog', 'eposi');}  ?></h2>
			</header>
			<div class="breadcrumb-container">
				<?php Eposi_Class::eposi_breadcrumb(); ?> 
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12 <?php echo 'col-lg-'.$eposi_blogcolclass; ?> <?php echo ''.$main_column_class; ?> <?php echo esc_attr($eposi_blog_main_extra_class);?>">
				<div class="page-content blog-page blogs <?php echo esc_attr($eposi_blogclass); ?>">
					<div class="blog-wrapper">
						<?php if ( have_posts() ) : ?>
							<div class="post-container">
								<?php /* Start the Loop */ ?>
								<?php while ( have_posts() ) : the_post(); ?>
									<?php get_template_part( 'content', get_post_format() ); ?>
								<?php endwhile; ?>
							</div>
							<?php Eposi_Class::eposi_pagination(); ?>
						<?php else : ?>
							<article id="post-0" class="post no-results not-found">
							<?php if ( current_user_can( 'edit_posts' ) ) :
								// Show a different message to a logged-in user who can add posts.
							?>
								<header class="entry-header">
									<h1 class="entry-title"><?php esc_html_e( 'No posts to display', 'eposi' ); ?></h1>
								</header>
								<div class="entry-content">
									<p><?php printf( wp_kses(__( 'Ready to publish your first post? <a href="%s">Get started here</a>.', 'eposi' ), array('a'=>array('href'=>array()))), admin_url( 'post-new.php' ) ); ?></p>
								</div><!-- .entry-content -->
							<?php else :
								// Show the default message to everyone else.
							?>
								<header class="entry-header">
									<h1 class="entry-title"><?php esc_html_e( 'Nothing Found', 'eposi' ); ?></h1>
								</header>
								<div class="entry-content">
									<p><?php esc_html_e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'eposi' ); ?></p>
									<?php get_search_form(); ?>
								</div><!-- .entry-content -->
							<?php endif; ?>
							</article><!-- #post-0 -->
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php if($eposi_bloglayout!='nosidebar' && is_active_sidebar('sidebar-1')): ?>
				<?php get_sidebar(); ?>
			<?php endif; ?>
		</div>
	</div>
	<!-- brand logo -->
	<?php 
		if(isset($eposi_opt['inner_brand']) && function_exists('eposi_brands_shortcode') && shortcode_exists( 'ourbrands' ) ){
			if($eposi_opt['inner_brand'] && isset($eposi_opt['brand_logos'][0]) && $eposi_opt['brand_logos'][0]['thumb']!=null) { ?>
				<div class="inner-brands">
					<div class="container">
						<?php echo do_shortcode('[ourbrands]'); ?>
					</div>
				</div>
			<?php }
		}
	?>
	<!-- end brand logo --> 
</div>
<?php get_footer(); ?>