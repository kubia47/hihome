<?php
/**
 * The template for displaying Tag pages
 *
 * Used to display archive-type pages for posts in a tag.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Eposi_Theme
 * @since Eposi 1.0
 */
$eposi_opt = get_option( 'eposi_opt' );
get_header();
$eposi_bloglayout = 'sidebar';
if(isset($eposi_opt['blog_layout']) && $eposi_opt['blog_layout']!=''){
	$eposi_bloglayout = $eposi_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$eposi_bloglayout = $_GET['layout'];
}
$eposi_blogsidebar = 'right';
if(isset($eposi_opt['sidebarblog_pos']) && $eposi_opt['sidebarblog_pos']!=''){
	$eposi_blogsidebar = $eposi_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$eposi_blogsidebar = $_GET['sidebar'];
}
if ( !is_active_sidebar( 'sidebar-1' ) )  {
	$eposi_bloglayout = 'nosidebar';
}
$eposi_blog_main_extra_class = NULl;
if($eposi_blogsidebar=='left') {
	$eposi_blog_main_extra_class = 'order-lg-last';
}
$main_column_class = NULL;
switch($eposi_bloglayout) {
	case 'sidebar':
		$eposi_blogclass = 'blog-sidebar';
		$eposi_blogcolclass = 9;
		$main_column_class = 'main-column';
		Eposi_Class::eposi_post_thumbnail_size('eposi-post-thumb');
		break;
	case 'largeimage':
		$eposi_blogclass = 'blog-large';
		$eposi_blogcolclass = 9;
		$main_column_class = 'main-column';
		Eposi_Class::eposi_post_thumbnail_size('eposi-post-thumbwide');
		break;
	case 'grid':
		$eposi_blogclass = 'grid';
		$eposi_blogcolclass = 9;
		$main_column_class = 'main-column';
		Eposi_Class::eposi_post_thumbnail_size('eposi-post-thumbwide');
		break;
	default:
		$eposi_blogclass = 'blog-nosidebar';
		$eposi_blogcolclass = 12;
		$eposi_blogsidebar = 'none';
		Eposi_Class::eposi_post_thumbnail_size('eposi-post-thumb');
}
?>
<div class="main-container page-wrapper">
	<div class="title-breadcumbs">
		<div class="container">
			<header class="entry-header">
				<h2 class="entry-title"><?php the_archive_title(); ?></h2>
			</header>
			<div class="breadcrumb-container">
				<?php Eposi_Class::eposi_breadcrumb(); ?> 
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12 <?php echo 'col-lg-'.$eposi_blogcolclass; ?> <?php echo ''.$main_column_class; ?> <?php echo esc_attr($eposi_blog_main_extra_class);?>">
				<div class="page-content blogs blog-page <?php echo esc_attr($eposi_blogclass); if($eposi_blogsidebar=='left') {echo ' left-sidebar'; } if($eposi_blogsidebar=='right') {echo ' right-sidebar'; } ?>">
					<?php if ( have_posts() ) : ?>
						<?php if ( tag_description() ) : // Show an optional tag description ?>
							<div class="archive-header">
								<h2 class="archive-title"><?php printf( wp_kses(__( 'Tag Archives: %s', 'eposi' ), array('span'=>array())), '<span>' . single_tag_title( '', false ) . '</span>' ); ?></h2>
								<div class="archive-meta"><?php echo tag_description(); ?></div>
							</div><!-- .archive-header -->
						<?php endif; ?>
						<div class="post-container">
							<?php
							/* Start the Loop */
							while ( have_posts() ) : the_post();
								/*
								 * Include the post format-specific template for the content. If you want to
								 * this in a child theme then include a file called called content-___.php
								 * (where ___ is the post format) and that will be used instead.
								 */
								get_template_part( 'content', get_post_format() );
							endwhile;
							?>
						</div>
						<?php Eposi_Class::eposi_pagination(); ?>
					<?php else : ?>
						<?php get_template_part( 'content', 'none' ); ?>
					<?php endif; ?>
				</div>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
	<!-- brand logo -->
	<?php 
		if(isset($eposi_opt['inner_brand']) && function_exists('eposi_brands_shortcode') && shortcode_exists( 'ourbrands' ) ){
			if($eposi_opt['inner_brand'] && isset($eposi_opt['brand_logos'][0]) && $eposi_opt['brand_logos'][0]['thumb']!=null) { ?>
				<div class="inner-brands">
					<div class="container">
						<?php echo do_shortcode('[ourbrands]'); ?>
					</div>
				</div>
			<?php }
		}
	?>
	<!-- end brand logo --> 
</div>
<?php get_footer(); ?>