<?php 
$array_imports = array(
	'home1' => array(
		'image' => 'screenshot1',
		'page_name' => 'Home shop 1',
		'is_default' => 1
	),
	'home2' => array(
		'image' => 'screenshot2',
		'page_name' => 'Home shop 2', 
	),
	'home3' => array(
		'image' => 'screenshot3',
		'page_name' => 'Home shop 3', 
	),
	'home4' => array(
		'image' => 'screenshot4',
		'page_name' => 'Home shop 4', 
	),
	'home5' => array(
		'image' => 'screenshot5',
		'page_name' => 'Home shop 5', 
	),
	'home6' => array(
		'image' => 'screenshot6',
		'page_name' => 'Home shop 6', 
	),
);
$opt_name = 'eposi_opt';