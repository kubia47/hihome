<?php
//Shortcodes for Visual Composer
add_action( 'vc_before_init', 'eposi_vc_shortcodes' );
function eposi_vc_shortcodes() { 
	//Site logo
	vc_map( array(
		'name' => esc_html__( 'Logo', 'eposi'),
		'description' => esc_html__( 'Insert logo image', 'eposi' ),
		'base' => 'roadlogo',
		'class' => '',
		'category' => esc_html__( 'Theme', 'eposi'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params' => array(
			array(
				'type'       => 'attach_image',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Upload logo image', 'eposi' ),
				'description'=> esc_html__( 'Note: For retina screen, logo image size is at least twice as width and height (width is set below) to display clearly', 'eposi' ),
				'param_name' => 'logo_image',
				'value'      => '',
			),
			array(
				'type' => 'dropdown',
				'holder' => 'div',
				'class' => '',
				'heading' => esc_html__( 'Insert logo link or not', 'eposi' ),
				'param_name' => 'logo_link',
				'value' => array(
					esc_html__( 'Yes', 'eposi' )	=> 1,
					esc_html__( 'No', 'eposi' )	=> 0,
				),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Logo width (unit: px)', 'eposi' ),
				'description'=> esc_html__( 'Insert number. Leave blank if you want to use original image size', 'eposi' ),
				'param_name' => 'logo_width',
				'value'      => esc_html__( '150', 'eposi' ),
			),
		)
	) );
	//Main Menu
	vc_map( array(
		'name'        => esc_html__( 'Main Menu', 'eposi'),
		'description' => esc_html__( 'Set Primary Menu in Apperance - Menus - Manage Locations', 'eposi' ),
		'base'        => 'roadmainmenu',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'eposi'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => '',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Set Primary Menu in Apperance - Menus - Manage Locations', 'eposi' ),
				'param_name' => 'no_settings',
			),
		),
	) );
	//Sticky Menu
	vc_map( array(
		'name'        => esc_html__( 'Sticky Menu', 'eposi'),
		'description' => esc_html__( 'Set Sticky Menu in Apperance - Menus - Manage Locations', 'eposi' ),
		'base'        => 'roadstickymenu',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'eposi'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => '',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Set Sticky Menu in Apperance - Menus - Manage Locations', 'eposi' ),
				'param_name' => 'no_settings',
			),
		),
	) );
	//Mobile Menu
	vc_map( array(
		'name'        => esc_html__( 'Mobile Menu', 'eposi'),
		'description' => esc_html__( 'Set Mobile Menu in Apperance - Menus - Manage Locations', 'eposi' ),
		'base'        => 'roadmobilemenu',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'eposi'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => '',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Set Mobile Menu in Apperance - Menus - Manage Locations', 'eposi' ),
				'param_name' => 'no_settings',
			),
		),
	) );
	//Wishlist
	vc_map( array(
		'name'        => esc_html__( 'Wishlist', 'eposi'),
		'description' => esc_html__( 'Wishlist', 'eposi' ),
		'base'        => 'roadwishlist',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'eposi'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => '',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'This widget does not have settings', 'eposi' ),
				'param_name' => 'no_settings',
			),
		),
	) );
	//Categories Menu
	vc_map( array(
		'name'        => esc_html__( 'Categories Menu', 'eposi'),
		'description' => esc_html__( 'Set Categories Menu in Apperance - Menus - Manage Locations', 'eposi' ),
		'base'        => 'roadcategoriesmenu',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'eposi'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => '',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Set Categories Menu in Apperance - Menus - Manage Locations', 'eposi' ),
				'param_name' => 'no_settings',
			),
		),
	) );
	//Social Icons
	vc_map( array(
		'name'        => esc_html__( 'Social Icons', 'eposi'),
		'description' => esc_html__( 'Configure icons and links in Theme Options', 'eposi' ),
		'base'        => 'roadsocialicons',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'eposi'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => '',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'This widget does not have settings', 'eposi' ),
				'param_name' => 'no_settings',
			),
		),
	) );
	//Mini Cart
	vc_map( array(
		'name'        => esc_html__( 'Mini Cart', 'eposi'),
		'description' => esc_html__( 'Mini Cart', 'eposi' ),
		'base'        => 'roadminicart',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'eposi'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => '',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'This widget does not have settings', 'eposi' ),
				'param_name' => 'no_settings',
			),
		),
	) );
	//Products Search without dropdown
	vc_map( array(
		'name'        => esc_html__( 'Product Search (No dropdown)', 'eposi'),
		'description' => esc_html__( 'Product Search (No dropdown)', 'eposi' ),
		'base'        => 'roadproductssearch',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'eposi'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => '',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'This widget does not have settings', 'eposi' ),
				'param_name' => 'no_settings',
			),
		),
	) );
	//Products Search with dropdown
	vc_map( array(
		'name'        => esc_html__( 'Product Search (Dropdown)', 'eposi'),
		'description' => esc_html__( 'Product Search (Dropdown)', 'eposi' ),
		'base'        => 'roadproductssearchdropdown',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'eposi'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => '',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'This widget does not have settings', 'eposi' ),
				'param_name' => 'no_settings',
			),
		),
	) );
	//Image slider
	vc_map( array(
		'name'        => esc_html__( 'Image slider', 'eposi' ),
		'description' => esc_html__( 'Upload images and links in Theme Options', 'eposi' ),
		'base'        => 'image_slider',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'eposi'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of rows', 'eposi' ),
				'param_name' => 'rows',
				'value'      => array(
					esc_html__( '1', 'eposi' )	=> '1',
					esc_html__( '2', 'eposi' )	=> '2',
					esc_html__( '3', 'eposi' )	=> '3',
					esc_html__( '4', 'eposi' )	=> '4',
				),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: over 1201px)', 'eposi' ),
				'param_name' => 'items_1200up',
				'value'      => esc_html__( '4', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: 992px - 1199px)', 'eposi' ),
				'param_name' => 'items_992_1199',
				'value'      => esc_html__( '4', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: 768px - 991px)', 'eposi' ),
				'param_name' => 'items_768_991',
				'value'      => esc_html__( '3', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: 640px - 767px)', 'eposi' ),
				'param_name' => 'items_640_767',
				'value'      => esc_html__( '2', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: 480px - 639px)', 'eposi' ),
				'param_name' => 'items_480_639',
				'value'      => esc_html__( '2', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: under 479px)', 'eposi' ),
				'param_name' => 'items_0_479',
				'value'      => esc_html__( '1', 'eposi' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Navigation', 'eposi' ),
				'param_name' => 'navigation',
				'value'      => array(
					esc_html__( 'Yes', 'eposi' ) => true,
					esc_html__( 'No', 'eposi' )  => false,
				),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Pagination', 'eposi' ),
				'param_name' => 'pagination',
				'value'      => array(
					esc_html__( 'No', 'eposi' )  => false,
					esc_html__( 'Yes', 'eposi' ) => true,
				),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Item Margin (unit: pixel)', 'eposi' ),
				'param_name' => 'item_margin',
				'value'      => esc_html__( '30', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Slider speed number (unit: second)', 'eposi' ),
				'param_name' => 'speed',
				'value'      => esc_html__( '500', 'eposi' ),
			),
			array(
				'type'       => 'checkbox',
				'value'      => true,
				'heading'    => esc_html__( 'Slider loop', 'eposi' ),
				'param_name' => 'loop',
			),
			array(
				'type'       => 'checkbox',
				'value'      => true,
				'heading'    => esc_html__( 'Slider Auto', 'eposi' ),
				'param_name' => 'auto',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style', 'eposi' ),
				'param_name' => 'style',
				'value'      => array(
					esc_html__( 'Style 1', 'eposi' )  => 'style1',
				),
			)
		),
	) );
	//Brand logos
	vc_map( array(
		'name'        => esc_html__( 'Brand Logos', 'eposi' ),
		'description' => esc_html__( 'Upload images and links in Theme Options', 'eposi' ),
		'base'        => 'ourbrands',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'eposi'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of rows', 'eposi' ),
				'param_name' => 'rows',
				'value'      => array(
					esc_html__( '1', 'eposi' )	=> '1',
					esc_html__( '2', 'eposi' )	=> '2',
					esc_html__( '3', 'eposi' )	=> '3',
					esc_html__( '4', 'eposi' )	=> '4',
				),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: over 1201px)', 'eposi' ),
				'param_name' => 'items_1201up',
				'value'      => esc_html__( '5', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: 992px - 1199px)', 'eposi' ),
				'param_name' => 'items_992_1199',
				'value'      => esc_html__( '5', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: 768px - 991px)', 'eposi' ),
				'param_name' => 'items_768_991',
				'value'      => esc_html__( '4', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: 640px - 767px)', 'eposi' ),
				'param_name' => 'items_640_767',
				'value'      => esc_html__( '3', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: 480px - 639px)', 'eposi' ),
				'param_name' => 'items_480_639',
				'value'      => esc_html__( '2', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: under 479px)', 'eposi' ),
				'param_name' => 'items_0_479',
				'value'      => esc_html__( '1', 'eposi' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Navigation', 'eposi' ),
				'param_name' => 'navigation',
				'value'      => array(
					esc_html__( 'Yes', 'eposi' ) => true,
					esc_html__( 'No', 'eposi' )  => false,
				),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Pagination', 'eposi' ),
				'param_name' => 'pagination',
				'value'      => array(
					esc_html__( 'No', 'eposi' )  => false,
					esc_html__( 'Yes', 'eposi' ) => true,
				),
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Item Margin (unit: pixel)', 'eposi' ),
				'param_name' => 'item_margin',
				'value'      => esc_html__( '0', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    =>  esc_html__( 'Slider speed number (unit: second)', 'eposi' ),
				'param_name' => 'speed',
				'value'      => esc_html__( '500', 'eposi' ),
			),
			array(
				'type'       => 'checkbox',
				'value'      => true,
				'heading'    => esc_html__( 'Slider loop', 'eposi' ),
				'param_name' => 'loop',
			),
			array(
				'type'       => 'checkbox',
				'value'      => true,
				'heading'    => esc_html__( 'Slider Auto', 'eposi' ),
				'param_name' => 'auto',
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Style', 'eposi' ),
				'param_name' => 'style',
				'value'      => array(
					esc_html__( 'Style 1', 'eposi' )       => 'style1',
				),
			)
		),
	) );
	//Latest posts
	vc_map( array(
		'name'        => esc_html__( 'Latest posts', 'eposi' ),
		'description' => esc_html__( 'List posts', 'eposi' ),
		'base'        => 'latestposts',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'eposi'),
		"icon"        => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of posts', 'eposi' ),
				'param_name' => 'posts_per_page',
				'value'      => esc_html__( '10', 'eposi' ),
			),
			array(
				'type'        => 'textfield',
				'holder'      => 'div',
				'class'       => '',
				'heading'     => esc_html__( 'Category', 'eposi' ),
				'param_name'  => 'category',
				'value'       => esc_html__( '0', 'eposi' ),
				'description' => esc_html__( 'Slug of the category (example: slug-1, slug-2). Default is 0 : show all posts', 'eposi' ),
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Image scale', 'eposi' ),
				'param_name' => 'image',
				'value'      => array(
					esc_html__( 'Wide', 'eposi' )	=> 'wide',
					esc_html__( 'Square', 'eposi' ) => 'square',
				),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Excerpt length', 'eposi' ),
				'param_name' => 'length',
				'value'      => esc_html__( '20', 'eposi' ),
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns', 'eposi' ),
				'param_name' => 'colsnumber',
				'value'      => array(
					esc_html__( '1', 'eposi' )	=> '1',
					esc_html__( '2', 'eposi' )	=> '2',
					esc_html__( '3', 'eposi' )	=> '3',
					esc_html__( '4', 'eposi' )	=> '4',
				),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => esc_html__( 'Enable slider', 'eposi' ),
				'param_name'  => 'enable_slider',
				'value'       => true,
				'save_always' => true, 
				'group'       => esc_html__( 'Slider Options', 'eposi' ),
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of rows', 'eposi' ),
				'param_name' => 'rowsnumber',
				'group'      => esc_html__( 'Slider Options', 'eposi' ),
				'value'      => array(
						esc_html__( '1', 'eposi' )	=> '1',
						esc_html__( '2', 'eposi' )	=> '2',
						esc_html__( '3', 'eposi' )	=> '3',
						esc_html__( '4', 'eposi' )	=> '4',
						esc_html__( '5', 'eposi' )	=> '5',
					),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: 992px - 1199px)', 'eposi' ),
				'param_name' => 'items_992_1199',
				'value'      => esc_html__( '3', 'eposi' ),
				'group'      => esc_html__( 'Slider Options', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: 768px - 991px)', 'eposi' ),
				'param_name' => 'items_768_991',
				'value'      => esc_html__( '3', 'eposi' ),
				'group'      => esc_html__( 'Slider Options', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: 640px - 767px)', 'eposi' ),
				'param_name' => 'items_640_767',
				'value'      => esc_html__( '2', 'eposi' ),
				'group'      => esc_html__( 'Slider Options', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: 480px - 639px)', 'eposi' ),
				'param_name' => 'items_480_639',
				'value'      => esc_html__( '2', 'eposi' ),
				'group'      => esc_html__( 'Slider Options', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of columns (screen: under 479px)', 'eposi' ),
				'param_name' => 'items_0_479',
				'value'      => esc_html__( '1', 'eposi' ),
				'group'      => esc_html__( 'Slider Options', 'eposi' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Navigation', 'eposi' ),
				'param_name'  => 'navigation',
				'save_always' => true,
				'group'       => esc_html__( 'Slider Options', 'eposi' ),
				'value'       => array(
					esc_html__( 'Yes', 'eposi' ) => true,
					esc_html__( 'No', 'eposi' )  => false,
				),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Pagination', 'eposi' ),
				'param_name'  => 'pagination',
				'save_always' => true,
				'group'       => esc_html__( 'Slider Options', 'eposi' ),
				'value'       => array(
					esc_html__( 'No', 'eposi' )  => false,
					esc_html__( 'Yes', 'eposi' ) => true,
				),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Item Margin (unit: pixel)', 'eposi' ),
				'param_name'  => 'item_margin',
				'value'       => esc_html__( '30', 'eposi' ),
				'save_always' => true,
				'group'       => esc_html__( 'Slider Options', 'eposi' ),
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Slider speed number (unit: second)', 'eposi' ),
				'param_name'  => 'speed',
				'value'       => esc_html__( '500', 'eposi' ),
				'save_always' => true,
				'group'       => esc_html__( 'Slider Options', 'eposi' ),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => esc_html__( 'Slider loop', 'eposi' ),
				'param_name'  => 'loop',
				'value'       => true,
				'group'       => esc_html__( 'Slider Options', 'eposi' ),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => esc_html__( 'Slider Auto', 'eposi' ),
				'param_name'  => 'auto',
				'value'       => true,
				'group'       => esc_html__( 'Slider Options', 'eposi' ),
			),
		),
	) );
	//Testimonials
	vc_map( array(
		'name'        => esc_html__( 'Testimonials', 'eposi' ),
		'description' => esc_html__( 'Testimonial slider', 'eposi' ),
		'base'        => 'testimonials',
		'class'       => '',
		'category'    => esc_html__( 'Theme', 'eposi'),
		"icon"     	  => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'      => array(
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number of testimonial', 'eposi' ),
				'param_name' => 'limit',
				'value'      => esc_html__( '10', 'eposi' ),
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Display Author', 'eposi' ),
				'param_name' => 'display_author',
				'value'      => array(
					esc_html__( 'Yes', 'eposi' )	=> '1',
					esc_html__( 'No', 'eposi' )	=> '0',
				),
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Display Avatar', 'eposi' ),
				'param_name' => 'display_avatar',
				'value'      => array(
					esc_html__( 'Yes', 'eposi' )=> '1',
					esc_html__( 'No', 'eposi' ) => '0',
				),
			),
			array(
				'type'        => 'textfield',
				'holder'      => 'div',
				'class'       => '',
				'heading'     => esc_html__( 'Avatar image size', 'eposi' ),
				'param_name'  => 'size',
				'value'       => esc_html__( '150', 'eposi' ),
				'description' => esc_html__( 'Avatar image size in pixels. Default is 150', 'eposi' ),
			),
			array(
				'type'       => 'dropdown',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Display URL', 'eposi' ),
				'param_name' => 'display_url',
				'value'      => array(
					esc_html__( 'Yes', 'eposi' )	=> '1',
					esc_html__( 'No', 'eposi' )	=> '0',
				),
			),
			array(
				'type'        => 'textfield',
				'holder'      => 'div',
				'class'       => '',
				'heading'     => esc_html__( 'Category', 'eposi' ),
				'param_name'  => 'category',
				'value'       => esc_html__( '0', 'eposi' ),
				'description' => esc_html__( 'Slug of the category (only one category). Default is 0 : show all testimonials', 'eposi' ),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Navigation', 'eposi' ),
				'param_name' => 'navigation',
				'value'      => array(
					esc_html__( 'Yes', 'eposi' ) => true,
					esc_html__( 'No', 'eposi' )  => false,
				),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => esc_html__( 'Pagination', 'eposi' ),
				'param_name' => 'pagination',
				'value'      => array(
					esc_html__( 'Yes', 'eposi' ) => true,
					esc_html__( 'No', 'eposi' )  => false,
				),
			),
			array(
				'type'       => 'textfield',
				'heading'    =>  esc_html__( 'Slider speed number (unit: second)', 'eposi' ),
				'param_name' => 'speed',
				'value'      => esc_html__( '500', 'eposi' ),
			),
			array(
				'type'       => 'checkbox',
				'value'      => true,
				'heading'    => esc_html__( 'Slider loop', 'eposi' ),
				'param_name' => 'loop',
			),
			array(
				'type'       => 'checkbox',
				'value'      => true,
				'heading'    => esc_html__( 'Slider Auto', 'eposi' ),
				'param_name' => 'auto',
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Style', 'eposi' ),
				'param_name'  => 'style',
				'value'       => array(
					esc_html__( 'Style 1', 'eposi' )                => 'style1',
					esc_html__( 'Style 2 (about page)', 'eposi' )   => 'style-about-page',
				),
			)
		),
	) );
	//Counter
	vc_map( array(
		'name'     => esc_html__( 'Counter', 'eposi' ),
		'description' => esc_html__( 'Counter', 'eposi' ),
		'base'     => 'eposi_counter',
		'class'    => '',
		'category' => esc_html__( 'Theme', 'eposi'),
		"icon"     => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'   => array(
			array(
				'type'        => 'attach_image',
				'holder'      => 'div',
				'class'       => '',
				'heading'     => esc_html__( 'Image icon', 'eposi' ),
				'param_name'  => 'image',
				'value'       => '',
				'description' => esc_html__( 'Upload icon image', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Number', 'eposi' ),
				'param_name' => 'number',
				'value'      => '',
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Text', 'eposi' ),
				'param_name' => 'text',
				'value'      => '',
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Style', 'eposi' ),
				'param_name'  => 'style',
				'value'       => array(
					esc_html__( 'Style 1', 'eposi' )   => 'style1',
				),
			),
		),
	) );
	//Heading title
	vc_map( array(
		'name'     => esc_html__( 'Heading Title', 'eposi' ),
		'description' => esc_html__( 'Heading Title', 'eposi' ),
		'base'     => 'roadthemes_title',
		'class'    => '',
		'category' => esc_html__( 'Theme', 'eposi'),
		"icon"     => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'   => array(
			array(
				'type'       => 'textarea',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Heading title element', 'eposi' ),
				'param_name' => 'heading_title',
				'value'      => esc_html__( 'Title', 'eposi' ),
			),
			array(
				'type'       => 'textarea',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Heading sub-title element', 'eposi' ),
				'param_name' => 'sub_heading_title',
				'value'      => '',
			),
			array(
				'type'       => 'textarea',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'Heading sub-title 2 element', 'eposi' ),
				'param_name' => 'sub_heading_title2',
				'value'      => '',
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Style', 'eposi' ),
				'param_name'  => 'style',
				'value'       => array(
					esc_html__( 'Style 1 (Default)', 'eposi' )    => 'style1',
					esc_html__( 'Style 2', 'eposi' )              => 'style2',
					esc_html__( 'Style 3 (Footer title)', 'eposi' )     => 'style3',
				),
			),
		),
	) );
	//Countdown
	vc_map( array(
		'name'     => esc_html__( 'Countdown', 'eposi' ),
		'description' => esc_html__( 'Countdown', 'eposi' ),
		'base'     => 'roadthemes_countdown',
		'class'    => '',
		'category' => esc_html__( 'Theme', 'eposi'),
		"icon"     => get_template_directory_uri() . "/images/road-icon.jpg",
		'params'   => array(
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'End date (day)', 'eposi' ),
				'param_name' => 'countdown_day',
				'value'      => esc_html__( '1', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'End date (month)', 'eposi' ),
				'param_name' => 'countdown_month',
				'value'      => esc_html__( '1', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'class'      => '',
				'heading'    => esc_html__( 'End date (year)', 'eposi' ),
				'param_name' => 'countdown_year',
				'value'      => esc_html__( '2020', 'eposi' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Style', 'eposi' ),
				'param_name'  => 'style',
				'value'       => array(
					esc_html__( 'Style 1', 'eposi' )      => 'style1',
				),
			),
		),
	) );
}
?>