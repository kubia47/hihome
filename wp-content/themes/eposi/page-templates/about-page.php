<?php
/**
 * Template Name: About Template
 *
 * Description: About page template
 *
 * @package WordPress
 * @subpackage Eposi_Theme
 * @since Eposi 1.0
 */
$eposi_opt = get_option( 'eposi_opt' );
get_header();
?>
<div class="main-container about-page">
	<div class="title-breadcumbs">
		<div class="container">
			<header class="entry-header">
				<h2 class="entry-title"><?php the_title(); ?></h2>
			</header>
			<div class="breadcrumb-container">
				<?php Eposi_Class::eposi_breadcrumb(); ?> 
			</div>
		</div>
	</div>
	<div class="about-container">
		<?php while ( have_posts() ) : the_post(); ?>
			<div class="container">
				<?php get_template_part( 'content', 'page' ); ?>
			</div>
		<?php endwhile; ?>
	</div>
	<!-- brand logo -->
	<?php 
		if(isset($eposi_opt['inner_brand']) && function_exists('eposi_brands_shortcode') && shortcode_exists( 'ourbrands' ) ){
			if($eposi_opt['inner_brand'] && isset($eposi_opt['brand_logos'][0]) && $eposi_opt['brand_logos'][0]['thumb']!=null) { ?>
				<div class="inner-brands">
					<div class="container">
						<?php echo do_shortcode('[ourbrands]'); ?>
					</div>
				</div>
			<?php }
		}
	?>
	<!-- end brand logo -->  
</div>
<?php get_footer(); ?>