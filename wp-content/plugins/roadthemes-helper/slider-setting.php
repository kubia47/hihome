<?php 

if( ! function_exists( 'road_get_slider_setting' ) ) {
	function road_get_slider_setting() {
		return array(
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Style', 'eposi' ),
				'param_name'  => 'style',
				'value'       => array(
					__( 'Grid view', 'eposi' )                    => 'product-grid',
					__( 'List view', 'eposi' )                    => 'product-list',
					__( 'Grid view with countdown', 'eposi' )     => 'product-grid-countdown',
				),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => __( 'Enable slider', 'eposi' ),
				'description' => __( 'If slider is enabled, the "column" ins General group is the number of rows ', 'eposi' ),
				'param_name'  => 'enable_slider',
				'value'       => true,
				'save_always' => true, 
				'group'       => __( 'Slider Options', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: over 1500px)', 'eposi' ),
				'param_name' => 'items_1500up',
				'group'      => __( 'Slider Options', 'eposi' ),
				'value'      => esc_html__( '4', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 1200px - 1499px)', 'eposi' ),
				'param_name' => 'items_1200_1499',
				'group'      => __( 'Slider Options', 'eposi' ),
				'value'      => esc_html__( '4', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 992px - 1199px)', 'eposi' ),
				'param_name' => 'items_992_1199',
				'group'      => __( 'Slider Options', 'eposi' ),
				'value'      => esc_html__( '4', 'eposi' ),
			), 
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 768px - 991px)', 'eposi' ),
				'param_name' => 'items_768_991',
				'group'      => __( 'Slider Options', 'eposi' ),
				'value'      => esc_html__( '3', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 640px - 767px)', 'eposi' ),
				'param_name' => 'items_640_767',
				'group'      => __( 'Slider Options', 'eposi' ),
				'value'      => esc_html__( '2', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 375px - 639px)', 'eposi' ),
				'param_name' => 'items_375_639',
				'group'      => __( 'Slider Options', 'eposi' ),
				'value'      => esc_html__( '2', 'eposi' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: under 374px)', 'eposi' ),
				'param_name' => 'items_0_374',
				'group'      => __( 'Slider Options', 'eposi' ),
				'value'      => esc_html__( '1', 'eposi' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Navigation', 'eposi' ),
				'param_name'  => 'navigation',
				'save_always' => true,
				'group'       => __( 'Slider Options', 'eposi' ),
				'value'       => array(
					__( 'No', 'eposi' )  => false,
					__( 'Yes', 'eposi' ) => true,
				),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Pagination', 'eposi' ),
				'param_name'  => 'pagination',
				'save_always' => true,
				'group'       => __( 'Slider Options', 'eposi' ),
				'value'       => array(
					__( 'No', 'eposi' )  => false,
					__( 'Yes', 'eposi' ) => true,
				),
			),
			array(
				'type'        => 'textfield',
				'heading'     => __( 'Item Margin (unit: pixel)', 'eposi' ),
				'param_name'  => 'item_margin',
				'value'       => 30,
				'save_always' => true,
				'group'       => __( 'Slider Options', 'eposi' ),
			),
			array(
				'type'        => 'textfield',
				'heading'     => __( 'Slider speed number (unit: second)', 'eposi' ),
				'param_name'  => 'speed',
				'value'       => '500',
				'save_always' => true,
				'group'       => __( 'Slider Options', 'eposi' ),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => __( 'Slider loop', 'eposi' ),
				'param_name'  => 'loop',
				'value'       => true,
				'group'       => __( 'Slider Options', 'eposi' ),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => __( 'Slider Auto', 'eposi' ),
				'param_name'  => 'auto',
				'value'       => true,
				'group'       => __( 'Slider Options', 'eposi' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Navigation Style', 'eposi' ),
				'param_name'  => 'navigation_style',
				'value'       => array(
					__( 'Style 1', 'eposi' ) => 'nav-style',
				),
				'group'       => __( 'Slider Options', 'eposi' ),
			),
		);
	}
}